# Ejercicio 5


from mysound import Sound


class SoundSin(Sound):
    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)  # Llamamos al constructor de la clase base
        self.sin(frequency, amplitude)  # Generamos la señal sinusoidal
