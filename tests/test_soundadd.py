# Ejercicio 8

import unittest

from soundops import SoundSin


class MyTestCase(unittest.TestCase):
    def test_add_different_durations(self):
        audio = SoundSin

        s1 = audio(0.5, 440, amplitude=1000)
        s2 = audio(1.0, 880, amplitude=1000)
        result = audio.soundadd(s1, s2)
        self.assertEqual(result.duration, 1.0)

    def test_add_same_sound(self):
        audio = SoundSin
        s1 = audio(0.5, 440, amplitude=1000)
        result = audio.soundadd(s1, s1)
        self.assertAlmostEqual(result.buffer[1000], 2 * s1.buffer[1000])

    def test_add_empty_sound(self):
        audio = SoundSin
        s1 = audio(0.0, 440, amplitude=0)
        s2 = audio(0.0, 880, amplitude=0)
        result = audio.soundadd(s1, s2)
        self.assertEqual(len(result.buffer), 0)


if __name__ == '__main__':
    unittest.main()
