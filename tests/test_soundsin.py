# Ejercicio 6

import unittest
from mysoundsin import SoundSin


class TestSoundSin(unittest.TestCase):
    def test_soundsin_creation(self):
        # Prueba la creación de objetos SoundSin
        duration = 1.0
        frequency = 440
        amplitude = 10000
        sound_sin = SoundSin(duration, frequency, amplitude)

        self.assertEqual(sound_sin.duration, duration, msg=None)
        self.assertEqual(len(sound_sin.buffer), int(duration * SoundSin.samples_second), msg=None)

    def test_soundsin_bars(self):
        # Prueba la función bars
        duration = 0.5
        frequency = 880
        amplitude = 20000
        sound_sin = SoundSin(duration, frequency, amplitude)

        # Verifica que la función bars devuelva una cadena no vacía
        bars_result = sound_sin.bars()
        self.assertNotEqual(bars_result, "")

    def test_duration(self):
        sound = SoundSin(10, 1000, 500)
        self.assertEqual(sound.duration,10,msg=None)

if __name__ == '__main__':
    unittest.main()
