#Ejercicio 4"

import unittest

import time
from mysound import Sound

class Testmio(unittest.TestCase):
    def test_sound_(self):
        # Medir el tiempo que lleva generar una señal de sonido de 10 segundos
        start_time = time.time()
        sound = Sound(10)
        sound.sin(440, 10000)
        end_time = time.time()

        elapsed_time = end_time - start_time


        # Establecer un límite de tiempo aceptable (en segundos)
        max_allowed_time = 15.0

        # Verificar que el tiempo transcurrido sea menor o igual al límite permitido
        self.assertLessEqual(elapsed_time, max_allowed_time)

if __name__ == '__main__':
    unittest.main()
